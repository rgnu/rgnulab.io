
module.exports = {
  title: 'R-GNU',
  description: 'IT, SOFTWARE LIBRE Y OTRAS VERDURAS',
  base: '/',
  dest: 'public',
  theme: 'ououe',
  plugins: [
    // permalink for posts
    ['blog-multidir', {
      postsDir: {
        posts: ':year/:month/:day/:slug'
      },
      paginationLimit: 10,
      postsSorter: ((prev, next) => {
        // DESC Order
        return prev.frontmatter.date < next.frontmatter.date ? 1 : -1;
      })
    }],
    ['@vuepress/google-analytics', {
        'ga': 'UA-10813912-3' // UA-00000000-0
    }],
    ['@vuepress/pwa', {
      serviceWorker: true,
      updatePopup: true
    }]
  ],


  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }],
    ['link', { rel: 'manifest', href: '/manifest.webmanifest' }],
    
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'default' }],
    ['meta', { name: 'apple-mobile-web-app-title', content: 'R-GNU: IT, SOFTWARE LIBRE Y OTRAS VERDURAS' }],
    ['link', { rel: 'apple-touch-icon', href: '/images/logo-192x192.png' }],
    ['link', { rel: 'apple-touch-startup-icon', href: '/images/logo-192x192.png' }],
    
    ['meta', { name: 'mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
  ],
  markdown: {
    anchor: {
      permalink: false,
      permalinkBefore: false
    }
  },
  themeConfig: {
    logo: '/images/logo-192x192.png',
    cover: {
      base: 'https://images.unsplash.com/photo-1576145790918-bdddd6d5ad21?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjExMDk0fQ&auto=format&fit=crop&w=1280&h=720&q=30',
      base: 'https://images.unsplash.com/photo-1555949963-ff9fe0c870eb?ixlib=rb-1.2.1&auto=format&fit=crop&w=1280&q=30'
    },
    defaultTheme: 'light',
    showThemeButton: false,
    useVssue: false,
    nav: [
      //{ text: 'Home', link: '/' },
      { text: 'Posts', link: '/posts/' },
      { text: 'Tags', link: '/tag/' }
    ],
    footer: [
      { text: 'Github', link: 'https://github.com/garciarodrigor' },
      { text: 'Gitlab', link: 'https://gitlab.com/rgnu' },
      { text: 'Linkedin', link: 'https://www.linkedin.com/in/rodrigo-garcia-30003421/' },
      { text: 'Twitter', link: 'https://twitter.com/garcia_rodrigor' },
      { text: 'Latest Posts', link: '/posts' },
    ],
    social: {
      github: 'https://github.com/garciarodrigor',
      twitter: 'https://twitter.com/garcia_rodrigor',
      linkedin: 'https://www.linkedin.com/in/rodrigo-garcia-30003421/'
    },
    postTime: {
      createTime: 'Created Time',
      lastUpdated: false,
      options: { dateStyle: 'medium' }
    }
  }
};
