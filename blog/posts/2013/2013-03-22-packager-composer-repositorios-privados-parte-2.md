---
title: Packager + Composer = repositorios privados – Parte 2
image: //images.unsplash.com/photo-1512418490979-92798cec1380?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=536&q=30
display: home
date: 2013-03-22
tags:
  - PHP
  - Composer
  - Programming
categories:
  - Programming
---

Siguiendo con la serie de posts referentes a packager. En esta segunda parte vamos a integrar packager y composer para poder crear un repositorio que tenga nuestros propios paquetes, pero además que contengan las dependencias de estos y otros paquetes que utilizamos en nuestros proyectos.

<!-- more -->

Recordamos que tenemos un repositorio con la siguiente estructura

```
.
├── packager.json
├── packages
│   └── packager-1.0.0.zip
└── packages.json
```

Ahora bien, supongamos que nuestros proyectos tienen dependencia con los siguientes componentes de symfony
```
symfony/console 2.1
symfony/finder 2.1
symfony/process 2.1
symfony/yaml 2.1
symfony/filesystem 2.1
```

Lo que vamos a necesitar ahora es crear un file llamado “composer.json” al nivel de “packages.json” donde declararemos nuestras dependencias y configuraremos a composer para que guarde los files que baja en el directorio donde nosotros alojamos nuestros paquetes (cache-files-dir=./packages)
```json
{
    "name": "rgnu.com.ar/repository",
    "description": "Download repository dependencies",
    "type": "repository",
    "license": "GPL-3.0",
    "version": "0.1.1",
    "keywords": [ "repository" ],

    "require": {
        "symfony/console": "2.1.*@stable",
        "symfony/finder": "2.1.*@stable",
        "symfony/process": "2.1.*@stable",
        "symfony/filesystem": "2.1.*@stable",
        "symfony/yaml": "2.1.*@stable"
    },

    "config": {
        "cache-files-dir": "./packages"
    }
}
```

Una ves realizado esto ejecutamos un composer update, para que este baje las dependencias
```shell
$ ../composer.phar update
Loading composer repositories with package information
Updating dependencies (including require-dev)
  - Installing symfony/console (v2.1.8)
    Downloading: 100%         

  - Installing symfony/finder (v2.1.8)
    Downloading: 100%         

  - Installing symfony/process (v2.1.8)
    Downloading: 100%         

  - Installing symfony/filesystem (v2.1.8)
    Downloading: 100%         

  - Installing symfony/yaml (v2.1.8)
    Downloading: 100%         

Writing lock file
Generating autoload files
```

Una vez realizado el update, podemos borrar tanto el directorio vendor que se creo, como el file composer.lock
```shell
$ rm -rf vendor composer.lock
```

Ahora nuestro repositorio debería tener el siguiente contenido
```
.
├── composer.json
├── packager.json
├── packages
│   ├── packager-1.0.0.zip
│   └── symfony
│       ├── console
│       │   └── 2.1.8.0-v2.1.8.zip
│       ├── filesystem
│       │   └── 2.1.8.0-v2.1.8.zip
│       ├── finder
│       │   └── 2.1.8.0-v2.1.8.zip
│       ├── process
│       │   └── 2.1.8.0-v2.1.8.zip
│       └── yaml
│           └── 2.1.8.0-v2.1.8.zip
└── packages.json
```

Ahora lo único que nos queda es volver a realizar el build del repositorio para que incluya los paquetes que composer acaba de bajar
```shell
$ ../packager.phar build:repository
Loading Config ./packager.json
Process Repository ./
Add package rgnu.com.ar/packager (1.0.0)
Add package symfony/yaml (v2.1.8)
Add package symfony/console (v2.1.8)
Add package symfony/filesystem (v2.1.8)
Add package symfony/process (v2.1.8)
Add package symfony/finder (v2.1.8)
```

En la próxima entrega veremos como automatizar todo el proceso con Jenkins, de esta forma solo nos queda mantener el file “composer.json” con las dependencias y agregar nuestros paquetes al repositorio.
