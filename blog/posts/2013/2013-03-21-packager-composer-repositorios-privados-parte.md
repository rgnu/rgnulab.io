---
title: Packager + Composer = repositorios privados
image: //images.unsplash.com/photo-1556228841-7c69921649bb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=536&q=30
display: home
date: 2013-03-21
tags:
  - PHP
  - Composer
  - Programming
categories:
  - Programming
---

Después de casi dos años de no postear nada, voy a tratar de armar una serie de posts relacionados con packager, una herramienta que se creó con el objetivo de mitigar algunas limitaciones y aspectos negativos del uso de composer y el repositorio que este usa por defecto packagist.org.

<!-- more -->

Hay dos aspectos que tratamos de solucionar con esta herramienta, el primero es poder generar un repositorio para composer que pudiera contener paquetes desarrollados internamente en la empresa, y el segundo es el problema de performance que actualmente tiene composer al realizar operaciones de update, install, search, etc, teniendo habilitado su repositorio por defecto. Este último lo logramos resolver al crear un repositorios que solo tiene aquellos paquetes que nos interesan, de esta forma reducimos considerablemente el tamaño del mismo.

Basta de tanto parloteo y pasemos a la acción. Abre una consola de comandos y crea un directorio ej: packager.test. Luego vamos a necesitar clonar el proyecto

```bash
$ git clone git://github.com/garciarodrigor/packager.git
```

Una vez que clonamos el proyecto, instalamos composer si es que no lo tenemos instalado.
```bash
$ curl -s https://getcomposer.org/installer | php
```
La estructura de directorios quedaría de la siguiente forma

```
.
├── composer.phar
└── packager
```

Ahora actualizamos las dependencias del proyecto.
```bash
$ cd packager/
$ ../composer.phar update
```

Esto nos deja el proyecto totalmente functional
```bash
$ bin/packager 
Packager version 1.0.0

Usage:
  [options] command [arguments]

Options:
  --help           -h Display this help message.
  --quiet          -q Do not output any message.
  --verbose        -v Increase verbosity of messages.
  --version        -V Display this application version.
  --ansi              Force ANSI output.
  --no-ansi           Disable ANSI output.
  --no-interaction -n Do not ask any interactive question.

Available commands:
  compile            Compile and create Packager phar file
  deploy             Deploy the current repository
  help               Displays help for a command
  list               Lists commands
build
  build:repository   Build a composer repository
deploy
  deploy:cloudfile   Deploy the current repository to RackSpace CloudFile
```

Luego generamos el phar para el packager
```bash
$ php -dphar.readonly=0 bin/packager compile
$ mv packager.phar ../
$ cd ..
$ chmod 755 packager.phar
````

Ahora ya contamos con las herramientas necesarias, para poder empezar a construir nuestro propio repositorio.
```
.
├── composer.phar
├── packager
└── packager.phar
```

Procedemos a crear un directorio donde se alojará nuestro repositorio y un directorio donde se alojaran nuestros paquetes.
```bash
$ mkdir repo && cd repo && mkdir packages
````

Ahora debemos crear el archivo de configuración para packager (packager.json)
```json
{
    "name": "Test Repository",
    "homepage": "http://example.com/repo/",
    "output": "./",
    "repositories": [
        { "type": "dir", "path": "./" }
    ]
}
```

Los datos más relevantes son el “homepage” que es la url que se usará para acceder al repositorio, “output” que es el directorio donde va a dejar el archivo con toda la información de los paquetes encontrados, y “repositories” que le indica los paths donde se encuentran paquetes.

Una vez realizada la configuración podemos realizar el build del repositorio
```shell
$ ../packager.phar build:repository
Loading Config ./packager.json
Process Repository ./
```

Vamos a encontrar que se generó el file “packages.json” que contendría la información de todos los paquetes que se encuentran en nuestro repositorio, en nuestro caso como no hay ningún paquete, el mismo se encuentra vacío.
```
.
├── packager.json
├── packages
└── packages.json
```

Para comenzar a armar nuestro repositorio de prueba vamos a agregar un paquete y volvemos a realizar el build del mismo.
```
$ wget https://github.com/garciarodrigor/packager/archive/master.zip -O packages/packager-1.0.0.zip
$ ../packager.phar build:repository
Loading Config ./packager.json
Process Repository ./
Add package rgnu.com.ar/packager (1.0.0)
```

Ahora nuestro repositorio contiene el paquete “rgnu.com.ar/packager” en su version “1.0.0″, esto se encuentra reflejado en el file “packages.json” que contiene lo siguiente
```json
{
    "packages": {
        "rgnu.com.ar\/packager": {
            "1.0.0": {
                "name": "rgnu.com.ar\/packager",
                "description": "Create a composer repository from a list of repositories",
                "type": "application",
                "license": "GPL-3.0",
                "version": "1.0.0",
                "keywords": ["composer"],
                "require": {
                    "php": ">=5.3.0",
                    "ext-spl": "*",
                    "symfony\/finder": "2.1.*@stable",
                    "symfony\/console": "2.1.*@stable",
                    "omissis\/php-cloudfiles": "1.7.*@stable"
                },
                "autoload": {
                    "psr-0": {
                        "Packager": "src\/"
                    }
                },
                "dist": {
                    "url": "http:\/\/example.com\/repo\/packages\/packager-1.0.0.zip",
                    "type": "zip"
                }
            }
        }
    }
}
```

Bueno hasta acá hemos visto como utilizar esta herramienta para generar un repositorio privado, el cuál luego podremos utilizar con composer.

En el próximo post veremos como integrar esta herramienta junto con composer para agregar a nuestro repositorio los paquetes que nos interesan desde el repo principal de composer, junto con todas sus dependencias.