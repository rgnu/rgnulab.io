---
title: Istio 101 - Instalando Istio
image: //images.unsplash.com/photo-1490253599483-a908627eb692?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=536&q=30
display: home
date: 2020-01-04
tags:
  - Istio
  - ServiceMesh
  - DevOps
categories:
  - Kubernetes
---

Con la idea de inaugurar el nuevo diseño del blog, voy a publicar una serie de articulos relacionados con **Istio**. En esta primera parte vamos a ver como realizar su instalación en un cluster de Kubernetes.

<!-- more -->

# Instalando Istio

En este primer módulo, vamos a instalar Istio en nuestro cluster

1. Si bien hay muchas formas de istalar **Istio**, algunas más simples que otras, en nuestro caso vamos a ir por la versión oficiol. Para ello vamos a bajar y agregar a nuestro PATH:`istioctl` CLI que nos provee un set de herramientas para administrar **Istio**.
   ```shell
   $ curl -sL https://raw.githubusercontent.com/istio/istio/release-1.4/release/downloadIstioCtl.sh | sh -
   $ export PATH=$PATH:$HOME/.istioctl/bin
   $ istioctl verify-install
   ```

2. La opción más simple es instalar el perfíl de configuración `default` de **Istio** usando el siguiente comando:
    ```sell
    $ istioctl manifest apply
    ```
    Este comando instala el perfíl `default` en el cluster definido por tu configuración de Kubernetes. El perfíl `default` es un buen punto de partida para establecer un ambiente de producción, no así el perfíl `demo` que está destinado a evaluar las diferentes características de **Istio**.

    Si deseamos habilitar la seguridad en el perfíl `default`, podés hacerlo definiendo los siguientes parámetros:
    ```shell
    $ istioctl manifest apply --set values.global.mtls.enabled=true --set values.global.controlPlaneSecurityEnabled=true
    ```
    En general, podés utilizar la opción `--set` en `istioctl` de la misma forma que lo harías con `Helm`. La única diferencia es que deberás agregarle el prefijo `values`, ya que esta es la forma de indicarle que esa configuración debe ser aplicada a `Helm`.

    #### Instalar un perfíl diferente
    Otro tipo de perfiles pueden ser instalados en el cluster pasando el nombre del mismo por medio de una parámetro. Por ejemplo, el siguiente comando puede ser usado para instalar el perfíl `demo`:
    ```shell
    $ istioctl manifest apply --set profile=demo
    ```

    #### Mostrar la lista de perfiles definidos
    Podés ver la lista de perfiles de configuración de **Istio** accesibles desde `istioctl` usando el comando:
    ```shell
    $ istioctl profile list
    Istio configuration profiles:
        minimal
        remote
        sds
        default
        demo
    ```

3. Para asegurarnos de que los servicios fueron deployados correctamente y poder continuar podemos ejecutar el siguiente comando (puede tomar algunos minutos).

    ```shell
    $ kubectl get svc -n istio-system
    ```

    Output:
    ```
    NAME                     TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                                                 AGE
    istio-citadel            ClusterIP      10.106.129.67    <none>        8060/TCP,15014/TCP                                                 28m
    istio-galley             ClusterIP      10.98.27.51      <none>        443/TCP,15014/TCP,9901/TCP,15019/TCP                                                 28m
    istio-ingressgateway     LoadBalancer   10.110.195.116   172.17.0.14   15020:31046/TCP,80:31990/TCP,443:30185/TCP,15029:31842/TCP,15030:31885/TCP,15031:30149/TCP,15032:31145/TCP,15443:31648/TCP   28m
    istio-pilot              ClusterIP      10.103.216.175   <none>        15010/TCP,15011/TCP,8080/TCP,15014/TCP                                                 28m
    istio-policy             ClusterIP      10.109.136.115   <none>        9091/TCP,15004/TCP,15014/TCP                                                 28m
    istio-sidecar-injector   ClusterIP      10.110.208.201   <none>        443/TCP                                                 28m
    istio-telemetry          ClusterIP      10.110.20.183    <none>        9091/TCP,15004/TCP,15014/TCP,42422/TCP                                                 28m
    prometheus               ClusterIP      10.99.222.1      <none>        9090/TCP                                                 28m
    ```

**Note: Si el servicio `istio-ingressgateway` está en estado `<pending>`, verifica que tu cluster puede crear servicios de tipo `LoadBalancer`**

4. Como último paso vamos a asegurarnos que los pods`istio-citadel-*`, `istio-ingressgateway-*`, `istio-pilot-*` y `istio-policy-*` están todos en estado **`Running`** para poder continuar.

    ```shell
    $ kubectl get pods -n istio-system
    ```
    Output:
    ```
    NAME                                      READY   STATUS    RESTARTS   AGE
    istio-citadel-6d6cbfdddb-6mrj7            1/1     Running   0          31m
    istio-galley-65bd4d4c96-rhj5d             2/2     Running   0          31m
    istio-ingressgateway-5b694f6978-95jrx     1/1     Running   0          31m
    istio-pilot-6777944bc8-94bkq              2/2     Running   0          31m
    istio-policy-77875887d4-hsgst             2/2     Running   1          31m
    istio-sidecar-injector-75ff97b4d8-7wrqg   1/1     Running   0          31m
    istio-telemetry-78d4dc589-z5rlf           2/2     Running   1          31m
    prometheus-586d4445c7-jpq66               1/1     Running   0          31m
    ```

    Antes de continuar, asegurate que todos los pods fueron deployados y se encuentran en estado **`Running`** o **`Completed`**. Si se encuentran en estado `pending`, esperá unos minutos para que la instalación y el deployment finalicen.

    Enhorabuena!!! **Istio** se encuentra instalado en nuestro cluster.

#### [Parte 2 - Deployar la aplicación de prueba Bookinfo con **Istio Proxy**](2020-01-19-istio-101-instalando-bookinfo.md)