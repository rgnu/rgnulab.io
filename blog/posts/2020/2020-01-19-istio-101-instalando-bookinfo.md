---
title: Istio 101 - Parte 2 - Deployar Bookinfo con Istio Proxy
image: //images.unsplash.com/photo-1519186235235-0571766aa3d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=536&q=30
display: home
date: 2020-01-19
tags:
  - Istio
  - ServiceMesh
  - DevOps
categories:
  - Kubernetes
---

En esta entrega vamos a instalar la aplicación de prueba `Bookinfo` de [**Istio**](https://istio.io/), y veremos como de forma automática inyectarle `Envoy` el proxy utilizado por **Istio** 

<!-- more -->

La aplicación está compuesta de cuatro microservicios separados que se utilizan para demostrar las diferentes funcionalidades de [**Istio**](https://istio.io/). Esta muestra información sobre un libro, la descripción, el detalle (ISBN, níumero de páginas, etc), y algunas reviews del mismo.

Los diferentes microservicios son:

* `productpage`. Este es el frontend de la aplicación, se encarga de llamar a los servicios `details` y `reviews` para renderizar la página principal.
* `details`. Contiene la información asociada al libro (descripción, ISBN, níumero de páginas, etc).
* `reviews`. Contiene información relacionada con las reviews de los usuarios. Este también llama el servicio de `ratings`.
* `ratings`. Contiene la información sobre el puntaje y está asociado a la review.

Hay 3 diferentes versiones del servicio de `reviews`:

* Version `v1`, no llama al servicio de `ratings`.
* Version `v2`, llama al servicio `ratings`, y muestra la información con estrellas negras de 1 a 5.
* Version `v3`, llama al servicio `ratings`, y muestra la información con estrellas rojas de 1 a 5.

La arquitectura final de la aplicación se puede ver en el siquiente diagrama:

![Istio Bookinfo](//istio.io/docs/examples/bookinfo/noistio.svg)

### Habilitando la inyección automática del sidecar Proxy
En [**Kubernetes**](https://kubernetes.io/), un sidecar es un contenedor en el POD cuyo propósito es agregar alguna funcionalidad al contenedor principal. Para [**Istio**](https://istio.io/), los proxies `Envoy` son deployados como sidecars en todos los PODs del deployment. Hay dos formas de inyectar el sidecar de [**Istio**](https://istio.io/) en un POD: manualmente usando la CLI the Istio [`istioctl`](2020-01-04-istio-101-instalando-istio.md) o automáticamente usando el componente **Istio sidecar injector** que es un [**Admission Controller**](https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/), que generalmente es instalado junto con [**Istio**](https://istio.io/). En nuestro caso vamos a usar este último método, para lo cuál seguiremos unos simples pasos.

1. Vamos a agregar una etiqueta o `label` al namespace `default` para habilitar la inyección automática en el mismo, corriendo el siguiente comando:
    
    ```shell
    $ kubectl label namespace default istio-injection=enabled
    ```
    
2. Luego podremos comprobar los namespaces que están habilitados con la inyección automática, con el siguiente comando:
    
    ```shell
    $ kubectl get namespace -L istio-injection
    ```
    
    Output:
    ```
    NAME             STATUS   AGE    ISTIO-INJECTION
    default          Active   271d   enabled
    istio-system     Active   5d2h
    ...
    ```


## Instalar la aplicación Bookinfo

El siguiente comando deploya los 4 servicios que componen la aplicación `Bookinfo`, incluidas las 3 versiones del servicio `reviews`, en el namespace `default` del cluster de Kubernetes que estemos utilizando. Dado que nosotros hemos habilitado la inyección automática, estos PODs también incluiran como sidecar un `Envoy` proxy.

1. Deployar los servicios.

    ```shell
    $ kubectl -n default apply -f https://raw.githubusercontent.com/istio/istio/release-1.4/samples/bookinfo/platform/kube/bookinfo.yaml
    ```

    Output:
    ```
    service/details created
    serviceaccount/bookinfo-details created
    deployment.apps/details-v1 created
    service/ratings created
    serviceaccount/bookinfo-ratings created
    deployment.apps/ratings-v1 created
    service/reviews created
    serviceaccount/bookinfo-reviews created
    deployment.apps/reviews-v1 created
    deployment.apps/reviews-v2 created
    deployment.apps/reviews-v3 created
    service/productpage created
    serviceaccount/bookinfo-productpage created
    deployment.apps/productpage-v1 created
    ```

2. Confirmamos que todos los servicios y PODs están correctamente definidos y corriendo:

    ```shell
    $ kubectl -n default get svc
    ```

    Output:
    ```
    NAME          TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
    details       ClusterIP   10.111.193.211   <none>        9080/TCP   13m
    kubernetes    ClusterIP   10.96.0.1        <none>        443/TCP    86m
    productpage   ClusterIP   10.98.231.27     <none>        9080/TCP   13m
    ratings       ClusterIP   10.97.141.60     <none>        9080/TCP   13m
    reviews       ClusterIP   10.103.30.131    <none>        9080/TCP   13m
    ...
    ```
    
    y

    ```shell
    $ kubectl -n default get pods
    ```

    Output:
    ```
    NAME                             READY   STATUS    RESTARTS   AGE
    details-v1-c5b5f496d-pl5j9       2/2     Running   0          14m
    productpage-v1-c7765c886-gx5hh   2/2     Running   0          14m
    ratings-v1-f745cf57b-7fgfq       2/2     Running   0          14m
    reviews-v1-75b979578c-wqpng      2/2     Running   0          14m
    reviews-v2-597bf96c8f-xx28g      2/2     Running   0          15m
    reviews-v3-54c6c64795-pbj56      2/2     Running   0          15m
    ...
    ```

    Se puede ver que todos los PODs de `Bookinfo` tienen 2 contenedores. Uno es el contenedor del servicio, y el otro es el sidecar proxy `Envoy`.

3. Para confirmar que la aplicación `Bookinfo` está corriendo, vamos a enviar un request con `curl` desde un POD, por ejemplo desde `ratings`:
    ```shell
    $ kubectl exec -it $(kubectl get pod -l app=ratings -o jsonpath='{.items[0].metadata.name}') -c ratings -- curl productpage:9080/productpage | grep -o "<title>.*</title>"
    ```

    Output:
    ```
    <title>Simple Bookstore App</title>
    ```
    
Enhorabuena!!! La aplicación `Bookinfo` está corriendo correctamente, y la misma se encuentra inyectada con el Istio Proxy `Envoy`. En la próxima entrega veremos como acceder a las métricas que son recolectadas por el proxy de forma automática.

Continuará...

<!-- #### [Continue to Exercise 4 - Telemetry](../exercise-4/README.md) -->