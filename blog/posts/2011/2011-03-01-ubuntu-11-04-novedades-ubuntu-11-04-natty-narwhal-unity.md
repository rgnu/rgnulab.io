---
title: Ubuntu 11.04 novedades
image: //images.unsplash.com/photo-1622046108442-04efb9dbbb1a?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=536&q=30
type: post
display: home
date: 2013-03-21
tags:
  - Ubuntu
  - Ubuntu 11.04
categories:
  - Linux
---

El próximo Ubuntu 11.04 nombre clave Natty Narwhal será lanzado en abril de este año. Va a incluir bastantes novedades, principalmente en la interface. La nueva versión de Ubuntu incluirá Unity.
<!-- more -->

En mi netbook el dash se ve a pantalla completa

![Novedades Unity 3.6.0 en Ubuntu 11.04](http://ubunlog.com/wp-content/uploads/2011/03/unity-4.6.0_1-500x349.png)

Otro pequeño cambio es el nuevo color de fondo para el GRUB, más acorde a los tonos generales de Ubuntu.

El reproductor de música que se incluye por defecto en Ubuntu (Rhythmbox) será reemplazado por Banshee.

Los cambios más importantes para el usuario serán la inclusión de Unity con el nuevo AppMenu. La nueva forma de mostrar los menus de cada aplicación (en la zona superior) ahorrará bastante espacio en la pantalla. Falta por ver cuantas aplicaciones serán compatibles con este sistema en el lanzamiento de Ubuntu 11.04, pero tras verlo en funcionamiento la impresión general es que puede ser un cambio positivo.

![Ubuntu 11.04 Novedades - Ubuntu 11.04 Natty Narwhal - Unity](http://lh6.ggpht.com/_1QSDkzYY2vc/TSeOzf1p58I/AAAAAAAACoI/6-8znYOgdOY/appmenu.png)

Al maximizar la ventana se elimina la barra superior de la aplicación, ya que se integra en el panel. Se gana espacio en la zona superior de la pantalla, aunque se pierde con el listado de aplicaciones en el lateral izquierdo. Teniendo en cuenta que los monitores actuales son en formato panorámico, puede ser una buena solución. Habrá que esperar al lanzamiento oficial para probarlo durante unas semanas y comprobar si este cambio agrada a los usuarios.

Teniendo en cuenta el revuelo que supuso mover los botones de cerrar, minimizar y maximizar a la izquierda de las ventanas en Ubuntu 10.04, es de esperar que el salto a Unity suponga un fuerte debate. El cambio de orientación de esos botones tiene ahora más sentido, ya que se integran en el panel superior justo al lado del menú de la aplicación abierta.

Para hacerse una idea de cómo funciona, les recomiendo que vean el siguiente vídeo. Reproduce el vídeo a pantalla completa para ver cómo funciona el nuevo AppMenu.
