---
title: Space-Tweet buenos tweets vs. Malos tweets en Node, Socket.IO, MooTools
image: https://images.unsplash.com/10/wii.jpg?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=536&q=30
type: post
display: home
date: 2010-10-01
tags:
  - Javascript
  - Node
  - Nodejs
  - Mootools
  - Socket.io
  - Twitter
categories:
  - Programming
---

Reportado en el blog de Twitter, Jacob Thornton a creado una demostración divertida sobre el sentimiento de los tweet con el clásico Space Invaders.
<!-- more -->

[![Space-Tweet](https://img.youtube.com/vi/xvDzLODyDBo/0.jpg)](https://youtu.be/xvDzLODyDBo "Space-Tweet")

Jacob buscaba aprender más sobre Node y desarrollo este proyecto utilizando Node.js, Socket.IO, MooTools, y el API de streaming de Twitter.