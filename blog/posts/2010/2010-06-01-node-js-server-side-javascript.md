---
title: Node.js – Server-Side JavaScript
image: //nettuts.s3.amazonaws.com/601_node/preview.jpg
display: home
date: 2010-06-25
tags:
  - Internet
  - Javascript
  - Node
  - Nodejs
  - Server
categories:
  - Programming
---

Si, ya no solamente para los navegadores web. Aunque existan varias implementaciones de javascript, entre ellas Rhino, Flusspferd, Narwhal (el equivalente de JRuby, IronRuby, MRI, etc) la que está tomando mayor tracción es **Node.js**.
<!-- more -->

![Nodejs](//nettuts.s3.amazonaws.com/601_node/preview.jpg#left)
**Node.js** es un framework en javascript para el desarrollo de aplicaciones web en el lado del servidor. A partir de todas las implementaciones mencionadas, en enero del 2009 Kevin Dangoor se creo el proyecto de CommonJS, con el fin de crear una especificación que cada implementación pueda cumplir y así tener un API I/O en común.

**Node.js** es un tanto diferente de otras soluciones, porque está orientado al evento en lugar de ser basado en threads. Web servers como Apache están diseñados en base al modelos de thread, porque utilizan un thread/process para cada request entrante. Mientras esto funciona correctamente para muchas aplicaciones, el módelo de threads no escala muy bien con demasiadas conexiones, como las que se necesitan para servir aplicaciones de tiempo real como Friendfeed o Google Wave.

**Node.js**, usa un modelo de event loop en lugar de threads, de esta forma puede escalar para manejar ciento de miles de conexiones concurrentes. **Node.js** toma ventaja del hecho de que los servidores pasan la mayoría del tiempo esperando por operaciones de I/O, como leer un file desde el disco, acceder a servicios web externos o esperar para que el upload de un file termine, porque estas operaciones son mucho más lentas que las realizadas en memoria. Todas las operaciones de I/O en **Node.js** son asincrónicas, de esta forma el server puede continuar procesando request entrantes mientras espera que las operaciones de I/O finalicen. JavaScript esta muy bien preparado para la programación orientada a eventos, porque tiene funciones anónimas y closures que permiten definir callbacks inline, además que los desarrolladores en JavaScript ya conocen como programar de esta forma. Este modelo event-based hace de **Node.js** muy rápido, y hace que escalar aplicaciones de tiempo real sea muy sencillo.

## Paso 1. Instalación
**Node.js** corre en sistemas tipo Unix, como Mac OS X, Linux, y FreeBSD. Desafortunadamente o no ;) , Windows no está soportado. Vas a necesitar utilizar la terminal para instalar y correr **Node.js**.

Abre una terminal y corre los siguientes comandos.
```sh
$ wget http://github.com/ry/node/tarball/v0.1.99
$ tar -zxvf ry-node-v0.1.99-0-ga620b72.tar.gzls
$ cd ry-node-87230e6
$ ./configure
$ make
```

## Paso 2. Hello World!
Toda nueva tecnología comienza con un `Hello World!` tutorial, así que nosotros vamos a crear un simple HTTP server que responda con ese mensaje. Primero tenemos que comprender el sistema de módulos de *Node.js*. En **Node.js**, los módulos encapsulan funcionalidad que debe ser cargada en orden para ser utilizada. Hay una gran variedad de módulos que ya vienen con **Node.js**. Nosotros podemos cargar estos módulos usando la función “require” de esta forma:
```js
var sys = require("sys");
```

Esto carga el módulo `sys`, que contiene funciones relacionadas con tareas como por ejemplo realizar un print a la terminal. Para usar una función que el módulo exporta, debemos llamarla sobre la variable a la cual asignamos el módulo, en nuestro caso `sys`.
```js
sys.puts("Hello World!");
```

Correr estas dos líneas es tan simple como correr el comando node con el filename del javascript como argumento.
```sh
$ ./node test.js
```
Esto imprimirá `“Hello World!”` a la línea de comando cuando se corra.
![Nodejs Hello World](//nettuts.s3.amazonaws.com/601_node/2.jpg)

## Paso 3. Hello World! Server
Para crear un HTTP server, crearemos un file `server.js` con el siguiente contenido.
```js
var sys = require("sys"),
    http = require("http");

http.createServer(function(request, response) {
    response.sendHeader(200, {"Content-Type": "text/html"});
    response.write("Hello World!");
    response.close();
}).listen(8080);

sys.puts("Server running at http://localhost:8080/");
```

Este script importa los módulos `sys` y `http`, y crea un HTTP server. La función anónima que es pasada a `http.createServer` va a ser llamada siempre que llegue un request al server. Una vez que el server es creado, el quedará esperando en el port `8080`. Cuando un request llega, primero enviamos los HTTP headers el tipo de contenido (`Content-Type`) y código estado `(Status code) 200 (OK)`. Luego enviamos `Hello World!` y cerramos la conexión. Como pueden ver nosotros debemos indicar explicitamente el cierre de la conexión. Esto hace muy sencillo realizar un stream de datos hacia el cliente sin cerrar la conexión.

Si corremos este script y apuntamos el browser a la url `http://localhost:8080/` deberíamos ver en el `Hello World!`
```sh
$ node ./server.js
```

![Nodejs Hello World Server](//nettuts.s3.amazonaws.com/601_node/3.jpg)
