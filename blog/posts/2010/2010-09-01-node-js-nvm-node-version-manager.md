---
title: Node.js – NVM (Node Version Manager)
image: //miro.medium.com/max/778/1*0HGTGpb0bdSo_xdlptb4-A.png
display: home
date: 2010-09-23
tags:
  - Javascript
  - Node
  - Nodejs
  - NVM
categories:
  - Programming
---

Dando vueltas por algunos sitios sobre **Node.js** me encuentro con esta utilidad **NVM** (Node Version Manager) que me resulto muy práctica a la hora de compilar e instalar diferentes versiones de **Node.js**.
<!-- more -->

## Instalación
Para instalarlo debemos estar seguro que disponemos de un compilador de c++ en nuestro sistema. Para OSX, XCode debería funcionar, para Ubuntu, el paquete build-essential es suficiente. También necesitaremos `git` si vamos a realizar un seguimiento del HEAD del proyecto.
```sh
$ git clone git://github.com/creationix/nvm.git ~/.nvm
```

Luego agregamos las siguientes lineas en nuestro profile de bash:
```sh
$ NVM_DIR=$HOME/.nvm
$ . $NVM_DIR/nvm.sh
$ nvm use
```

La primera linea carga la función nvm en nuestro shell, dejandola disponible como un comando. La segunda linea configura nuestra versión por defecto de node a la última release.

## Uso
Para bajar, compilar, instalar y usar la release v0.1.94 release de node, debemos hacer los siguiente:
```sh
$ nvm install v0.1.94
$ nvm use v0.1.94
```

Si deseamos realizar un seguimiento del HEAD, utilizamos el comando clone:
```sh
$ nvm clone
$ nvm use HEAD
```

Cuando deseamos realizar una actualización del código desde el repositorio de node:
```sh
$ nvm update
```

Para ver las diferentes versiones de node que tenemos instaladas vamos a utilizar el comando list:
```sh
$ nvm list
```

Con estos sencillos pasos, estaremos en unos minutos experimentando con está nueva plataforma de desarrollo.