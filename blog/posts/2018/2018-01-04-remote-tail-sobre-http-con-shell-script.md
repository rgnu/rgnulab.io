---
title: Remote Tail sobre HTTP con Shell Script
image: //images.unsplash.com/photo-1486312338219-ce68d2c6f44d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=536&q=30
display: home
date: 2018-01-04
tags:
  - Bash
  - Tools
  - Shell Script
  - DevOps
  - AWK
  - Curl
categories:
  - Programming
---

Surgió un problemática en el trabajo, debíamos debuguear lo que estaba sucediendo en unos Solr que se encontraban en el datacenter y la única via de acceso a los logs era por medio de un webserver.

<!-- more -->

Seguramente alguien desarrollo alguna herramienta para hacer los mismo que hacemos con nuestro querido `tail`, pero con files que se encontraban accesible via HTTP. Pero lamentablemente o no, yo no encontre ninguna, y lo primero que se me vino a la mente es el `resume`, que tienen algunas utilidades de download.

Así que hechando mano a algunas herramientas disponible en la mayoría de los UNIX (`bash`, `curl`, `awk`, `wc`, `cat`), y después de un rato de darle vuelta salió este engendro, que si bien no tiene mucho diseño, ni patrones, ni nada por el estilo, increiblemente funciona :) . Por si alguno tiene la misma problemática acá les dejo esta pequeña porción de código, espero les sirva
```sh
URL='http://example.com/access.log'; \
SIZE=$(curl -I "$URL" 2> /dev/null | awk '/Content-Length:/ {printf("%d", $2)}'); \
while [ 1 ]; do \
curl -r$SIZE- "$URL" > /dev/null > out; cat out; \
LEN=$(cat out | wc -c); \
SIZE=$(expr $SIZE + $LEN ); \
sleep 1; \
done
```

La salida se puede combinar con otro comando (siguiendo la filosofía UNIX)
```sh
URL='http://example.com/access.log'; \
SIZE=$(curl -I "$URL" 2> /dev/null | awk '/Content-Length:/ {printf("%d", $2)}'); \
while [ 1 ]; do \
curl -r$SIZE- "$URL" 2> /dev/null > out; \
cat out; \
LEN=$(cat out | wc -c); \
SIZE=$(expr $SIZE + $LEN ); \
sleep 1; \
done | grep foo
```