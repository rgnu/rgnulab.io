## RGNU Blog

This blog was created with Vuepress static site generator and hosted via Gitlab Pages

### Installing dependencies
```
$ npm install
```

### Running in development mode
```
$ npm run dev
```

### Build the static site
```
$ npm run build
```
